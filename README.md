# Layers Catalog

## Inroduce

This is an open-source project designed to explore, add and manage [GIS](https://en.wikipedia.org/wiki/Geographic_information_system) layers information.

## Usage

### App GUI

The main screen of the site it a list of layers, sorted as if the first showing is the last-modified.

<img src="readme_images/home.png" height="300" />

You can create a layer by filling its information step by step:

<img src="readme_images/add.png" height="300" />

Once a layer is created, it is added into our database and can be explored by everyone. <br />
However, you are given management tools such as **edit** its data, as well as to **delete** the layer from the system.

<img src="readme_images/manage.png" height="300" />

### Using our uploading service

If you have your own layers repository and you want to add some of them into our catalog, you can use our service according to our requirements. <br />
Our service excepts for an object composited of two fields:

```
{
    URLs: ['http://layers_repository.com/'],
    parser: (arr) => foo(arr)
}
```


1.  `array` **URLs**: an array stores all URLs relevant to be parsed by the *parse* function.
2.  `function` **parser(arr)**: a function that recieves paramter `arr` of type `array` and manipluate it to finally **return** a layer `object` that matches our DB standards.

A final object as returned from the `parser` function must have **ALL** fields filled as described below:
```
{
    thumbnail: string,
    source_id: string,
    name: string,
    description: string,
    source: string,
    geographic_extent: GeoJSON,
    geographic_reference_system: integer,
    type: ENUM_TYPE,
    category: ENUM_CATEGORY,
    status: ENUM_STATUS,
    connection_metadata: {
        protocol: string,
        capabilitiesUrl: string,
    },
    version: integer,
    last_modified: date,
    date_range: {
        start: date,
        end: date,
    }
}
```

Add your configuration to the file located at `/Server/extenalLayersConfig.js`. <br />


## Technologies

This is a web-application based mainly on [React](https://reactjs.org/) in the frontend and [Node.js](https://nodejs.org/en/) in the backend. <br />
For the full list of packages used, check out the *package.json* file, located both in client & server folders.

## Notes

Currently the project **is in progress**, so that shown screenshots might be outdated. <br />
This also means that *debug*, *test* and *dev* enviornments are available to compile & run. Check out the **package.json** files to view the compile scripts. <br />
**Be aware**: this application is maintained in Ubuntu 16.04. Running terminal scripts on a different OS might result conflicts.