const Sequelize = require('sequelize')
const db = require('../dal/config')

const Layer = db.define('layers', {
    source_id: {
        type: Sequelize.STRING
    },
    thumbnail: {
        type: Sequelize.TEXT
    },
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    geographic_extent: {
        type: Sequelize.JSON
    },
    geographic_reference_system: {
        type: Sequelize.INTEGER
    },
    date_range: {
        type: Sequelize.JSON
    },
    last_modified: {
        type: Sequelize.DATE
    },
    type: {
        type: Sequelize.ENUM('vector', 'raster', '3d')
    },
    source: {
        type: Sequelize.STRING
    },
    category: {
        type: Sequelize.ENUM('discrete', 'unified')
    },
    connection_metadata: {
        type: Sequelize.JSON
    },
    version: {
        type: Sequelize.INTEGER
    },
    status: {
        type: Sequelize.ENUM('up-to-date', 'newer version available')
    },
  },
    {
        timestamps: false,
        freezeTableName: true
    })

module.exports = Layer;