const express = require('express');
const cors = require('cors')
// const {client} = require('./consts')

const app = express();

if (process.env.NODE_ENV === 'debug') {
    app.use(require('morgan')('dev'));
}
app.use(express.json({limit: '50mb'}));
app.use(express.urlencoded({ limit: '50mb', extended: true }));
app.use(cors({origin: '*'}))
app.use('/', require('./routes/routes'));


module.exports = app;
