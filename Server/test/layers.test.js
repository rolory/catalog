const chai = require('chai')
const should = chai.should()
const server = require('../app')
const { OK, NOT_FOUND, BAD_REQUEST } = require('http-status')
const Layer = require('../models/Layer')
const postObject = require('./layer_example')

chai.use(require(`chai-http`))


describe('Layers Tests', () => {

    // Delete all records from layers table
    after(() => {
        Layer.destroy({
            where: {},
            truncate: true
        })
    })

    it('should get all layers', (done) => {
        chai.request(server)
            .get('/layers')
            .end((err, res) => {
                res.should.have.status(OK)
                done()
            })
    })

    it('should return a 404 for a non-exisiting layer', (done) => {
        chai.request(server)
            .get('/layer/-1')
            .end((err, res) => {
                res.should.have.status(NOT_FOUND)
                done()
            })
    })

    it('should create a layer', (done) => {
        chai.request(server)
            .post('/layer')
            .send(
                postObject
            )
            .end((err, res) => {
                res.should.have.status(OK)
                done()
            })
    })


    it('should delete a layer', (done) => {
        chai.request(server).post('/layer')
            .send(postObject)
            .end((err, res) => {
                chai.request(server).delete(`/layer/${res.body.id}`)
                    .end((err, res) => {
                        res.should.have.status(OK)
                        done()
                    })
            })
    })
})