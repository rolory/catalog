const express = require('express');
const controller = require('./routes-controller')

const router = express.Router();
router.get('/layers', controller.getAllLayers)
router.post('/layer', controller.addNewLayer)
router.get('/layer/:id', controller.getLayerById)
router.get('/db_types/categories', controller.getCategoties)
router.get('/db_types/statuses', controller.getStatuses)
router.get('/db_types/types', controller.getTypes)
router.patch('/layer/:id', controller.updateLayerById)
router.delete('/layer/:id', controller.deleteLayerById)

module.exports = router;