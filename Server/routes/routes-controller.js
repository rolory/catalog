const getAllLayers = require('../bl/Layer/getAllLayers')
const addNewLayer = require('../bl/Layer/addNewLayer')
const getLayerById = require('../bl/Layer/getLayerById')
const updateLayerById = require('../bl/Layer/updateLayerById')
const deleteLayerById = require('../bl/Layer/deleteLayerById')
const getCategoties = require('../bl/DB Types/getCategories')
const getStatuses = require('../bl/DB Types/getStatuses')
const getTypes = require('../bl/DB Types/getTypes')

module.exports = {
    getAllLayers,
    addNewLayer,
    getLayerById,
    getCategoties,
    getStatuses,
    getTypes,
    updateLayerById,
    deleteLayerById
}