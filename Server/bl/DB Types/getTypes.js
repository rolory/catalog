const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const getTypes = async (req, res, next) => {
    try {
        res.send(Layer.rawAttributes.type.values).status(OK)
    } catch (err) {
        console.log(`Could not get enum: TYPE`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = getTypes;