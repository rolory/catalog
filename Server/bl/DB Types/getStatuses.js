const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const getStatuses = async (req, res, next) => {
    try {
        res.send(Layer.rawAttributes.status.values).status(OK)
    } catch (err) {
        console.log(`Could not get enum: STATUS`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = getStatuses;