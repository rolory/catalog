const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const getCategories = async (req, res, next) => {
    try {
        res.send(Layer.rawAttributes.category.values).status(OK)
    } catch (err) {
        console.log(`Could not get enum: CATEGORY`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = getCategories;