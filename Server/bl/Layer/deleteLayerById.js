const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const deleteLayerById = async (req, res, next) => {
    try {
        const id = req.params.id
        const layer = await Layer.findByPk(id)
        await layer.destroy()
        res.sendStatus(OK)
        
    } catch (err) {
        console.log(`Could not get layers`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = deleteLayerById;