const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST, NOT_FOUND} = require('http-status')

const getLayerById = async (req, res, next) => {
    try {
        const id = req.params.id
        const getLayer = await Layer.findByPk(id)
        if (getLayer !== null) // Checks if layer exsits
            res.json(getLayer).status(OK)
        else
            res.sendStatus(NOT_FOUND)
    } 
    catch (err) {
        console.log(`Could not get layers`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = getLayerById;