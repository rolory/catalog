const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const getAll = async (req, res, next) => {
    try {
        const getAll = await Layer.findAll({
            order: [
                ['last_modified', 'DESC']
            ]
        })
        res.json(getAll).status(OK)
    } catch (err) {
        console.log(`Could not get layers`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = getAll;