const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')

const updateLayerById = async (req, res, next) => {
    try {
        const id = req.params.id
        const layer = await Layer.findByPk(id)
        const updatedLayer = req.body

        Object.keys(updatedLayer).map(key =>
            layer[key] = updatedLayer[key])

        layer.last_modified = new Date()
            
        await layer.save()
        res.sendStatus(OK)
        
    } catch (err) {
        console.log(`Could not get layers`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = updateLayerById;