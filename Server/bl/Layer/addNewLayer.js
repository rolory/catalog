const Layer = require('../../models/Layer')
const {OK, BAD_REQUEST} = require('http-status')
const GJV = require('geojson-validation')

const addNewLayer = async (req, res, next) => {
    try {
        if (!GJV.valid(req.body.geographic_extent))
            throw new Error("Not a GEOJSON file")
        const layer = await Layer.create(req.body)
        res.json({id: layer.id}).status(OK)
    } catch (err) {
        console.log(`Could not add a new layer`)
        console.log(err)
        res.sendStatus(BAD_REQUEST)
    }
}

module.exports = addNewLayer;