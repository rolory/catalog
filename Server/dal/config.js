const Sequelize = require('sequelize');
const env = process.env.NODE_ENV
let seqBaseConfig = {
    host: 'localhost',
    dialect: 'postgres',
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
    logging: env === 'debug' ? console.log : false
};

if (env === 'debug') {
    sequelize = new Sequelize('postgres', 'admin', 'admin', seqBaseConfig)
}
if (env === 'production' || env === 'dev') {
    sequelize = new Sequelize('postgres', 'admin', 'admin', seqBaseConfig);
} if (env === 'test') {
    sequelize = new Sequelize('postgrestest', 'admin', 'admin', seqBaseConfig);
}

module.exports = sequelize