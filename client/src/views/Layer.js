import React from 'react';
import axios from 'axios'
import { server } from '../consts'
import DetailsTable from '../components/DetailsTable'
import { Row, Col, Image, Container, Button, Spinner } from 'react-bootstrap'
import { Download } from 'react-bootstrap-icons'
import { Link, Redirect } from 'react-router-dom'
import { InfoFill, Pencil, Check, X } from 'react-bootstrap-icons'
import SweetAlert from 'react-bootstrap-sweetalert';

class Layer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            layer: null,
            editMode: false,
            statuses: null,
            types: null,
            swal: {
                type: null,
                title: null,
                show: false,
            }
        };
        this.child = React.createRef()
    }

    toggleEditMode = () => {
        if (this.state.editMode) {
            const updateStatus = this.child.current.updateLayer()
            this.setState({ editMode: !updateStatus })
        }
        else
            this.setState({ editMode: true })
    }

    componentDidMount() {
        const { id } = this.props.match.params

        axios.get(`${server}/layer/${id}`)
            .then(res => this.setState({ layer: res.data }))
            .catch(() => this.setState({ layer: [] }))

        axios.get(`${server}/db_types/statuses`)
            .then(res => this.setState({ statuses: res.data }))

        axios.get(`${server}/db_types/types`)
            .then(res => this.setState({ types: res.data }))

    }

    downloadJsonHref = (o) => {
        return `data:text/json;charset=utf-8,${encodeURIComponent(JSON.stringify(o))}`
    }

    componentDidLoad = () => {
        return (
            this.state.layer &&
            this.state.statuses &&
            this.state.types
        )
    }

    deleteLayer = async () => {
        axios.delete(`${server}/layer/${this.state.layer.id}`)
            .then(() =>
                this.setSwal('success', `Layer ${this.state.layer.name} deleted`, true))
            .catch(() =>
                this.setSwal('danger', `Layer ${this.state.layer.name} could not be deleted`, true))
    }

    setSwal = (type, title, show) => {
        this.setState({
            swal: { type, title, show }
        })
    }

    hideAlert = () => {
        const type = this.state.swal.type

        this.setState({
            swal: {
                type: null,
                title: null,
                show: false,
                delete: false
            }
        })
        if (type === 'success')
            this.props.history.push('/')
    }

    requestedLayerExists = () => {
        return Object.keys(this.state.layer).length
    }

    render() {
        return (
            this.componentDidLoad() ?
                this.requestedLayerExists() ?
                    <Container fluid>
                        <Row>
                            <Col xs md={2}>
                            </Col>
                            <Col xs={12} md={8}>
                                <Row>
                                    <Col xs={12} md={4} className="text-center">
                                        <small>
                                            <Link to="/">Home Page</Link>
                                            <span> / </span>
                                            <strong>
                                                Layer {this.state.layer.id} - {this.state.layer.name}
                                            </strong>
                                        </small>
                                        <Image
                                            src={this.state.layer.thumbnail}
                                            width='100%'
                                            thumbnail />
                                    </Col>
                                    <Col xs={12} md={8}
                                        className="align-self-center text-center text-dark">
                                        <h1 className="display-md-3 font-weight-bold">
                                            {this.state.layer.name}
                                        </h1>
                                        <p className="lead">
                                            {this.state.layer.description}
                                        </p>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={12} md={4}>
                                        <Button
                                            block
                                            href={this.downloadJsonHref(this.state.layer)}
                                            download={`layer_${this.state.layer.id}.json`}
                                            variant="outline-dark"
                                            className="font-weight-bold">
                                            <Download size={20} /> Download JSON
                                    </Button>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs md={2}></Col>
                        </Row>
                        <Row>
                            <Col xs={12}>
                                <hr className="invisible" />
                            </Col>
                        </Row>
                        <Row>
                            <Col xs md={2}></Col>
                            <Col xs={12} md>
                                <h1 className="text-dark">
                                    <span ><InfoFill size={50} /></span> Layer Information
                            </h1>
                            </Col>
                            <Col xs={12} md className="align-self-end text-right">
                                <Row className="justify-content-end mb-1">
                                    <Col xs={12} md={4}>
                                        <div className="text-dark" onClick={this.toggleEditMode}>
                                            {
                                                this.state.editMode ?
                                                    <Button block variant="outline-success">
                                                        <Check size={20} /> finish editing
                                                </Button>
                                                    :
                                                    <Button block variant="outline-primary">
                                                        <Pencil size={20} /> edit info
                                                </Button>
                                            }
                                        </div>
                                    </Col>
                                </Row>
                                <Row className="justify-content-end">
                                    <Col xs={12} md={4}>
                                        <Button block variant="outline-danger"
                                            onClick={() => this.setState({ swal: { show: true, delete: true } })}>
                                            <X size={20} /> delete layer
                                    </Button>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs md={2}></Col>
                        </Row>
                        <Row className="mt-4 justify-content-center">
                            <Col xs md={8}>
                                <DetailsTable ref={this.child}
                                    types={this.state.types} statuses={this.state.statuses}
                                    editMode={this.state.editMode} layer={this.state.layer} />
                            </Col>
                        </Row>
                        {
                            this.state.swal.show ?
                                this.state.swal.delete ?
                                    <SweetAlert
                                        warning
                                        showCancel
                                        confirmBtnText="Delete"
                                        confirmBtnBsStyle="danger"
                                        title="Are you sure?"
                                        onConfirm={this.deleteLayer}
                                        onCancel={this.hideAlert}
                                        focusCancelBtn />
                                    :
                                    <SweetAlert
                                        type={this.state.swal.type}
                                        title={this.state.swal.title}
                                        onConfirm={this.hideAlert} />
                                : null
                        }
                    </Container>
                    : <Redirect to="/404" />
                :
                this.state.layer ?
                    <Redirect to="/" />
                    :
                    <React.Fragment>
                        <Spinner className="mr-3" animation="border" role="status" />
                        <span>Loading...</span>
                    </React.Fragment>

        );
    }
}

export default Layer