import React, { Component } from 'react';
import axios from 'axios'
import { server } from '../consts'
import { Badge, Container, Row, Col, Spinner } from 'react-bootstrap'
import ShowLayer from '../components/ShowLayer'
import CreateLayer from '../components/CreateLayer'
import LayersFilter from '../components/LayersFilter'
import ServerDown from '../components/ServerDown'


class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      layers: null,
      textFilterValue: '',
      categoryFilterValue: '',
      statusFilterValue: '',
      typeFilterValue: ''
    };
  }

  componentDidMount() {
    axios.get(`${server}/layers`)
    .then(res => this.setState({ layers: res.data, layerExists: true }))
    .catch(() => this.setState({ layers: [], layerExists: false }))
  }

  componentDidLoad = () => {
    return this.state.layers
  }

  updateTextFilter = (event) => {
    this.setState({ textFilterValue: event.target.value })
  }

  updateCategoryFilter = (event) => {
    this.setState({ categoryFilterValue: event.target.value })
  }

  updateStatusFilter = (event) => {
    this.setState({ statusFilterValue: event.target.value })
  }

  updateTypesFilter = (event) => {
    this.setState({ typeFilterValue: event.target.value })
  }

  filterCompareFunction = (a, b) => {
    return a.toLowerCase().includes(b.toLowerCase())
  }

  layersFiltered = () => {
    return this.state.layers.filter(layer =>
      (
        this.filterCompareFunction(layer.name, this.state.textFilterValue) ||
        this.filterCompareFunction(layer.description, this.state.textFilterValue))
      && this.filterCompareFunction(layer.category, this.state.categoryFilterValue)
      && this.filterCompareFunction(layer.status, this.state.statusFilterValue)
      && this.filterCompareFunction(layer.type, this.state.typeFilterValue)
    )
  }

  render() {
    return (
      this.componentDidLoad() ?
        <Container fluid className="mt-3">
          {
            this.state.layerExists ?
              <React.Fragment>
                <Row>
                  <Col xs={0} md={2} />
                  <Col xs={12} md={10}>
                    <Row>
                      <Col xs={6} md={10} className="text-center">
                        <h1>
                          Found <Badge variant="dark">{this.layersFiltered().length}</Badge> Layers
                        </h1>
                        {
                          [this.state.textFilterValue,
                          this.state.categoryFilterValue,
                          this.state.statusFilterValue,
                          this.state.typeFilterValue]
                            .map((filterKey, index) =>
                              <h4 className="d-inline-block mr-1" key={index}>
                                <Badge pill variant="dark">{filterKey}</Badge>
                              </h4>
                            )
                        }
                      </Col>
                      <Col xs={6} md={2}>
                        <CreateLayer />
                      </Col>
                      <hr />
                    </Row>
                  </Col>
                </Row>
                <Row>
                  <Col xs md={1} />
                  <Col xs={12} md={2}>
                    <LayersFilter
                      filterCategory={this.updateCategoryFilter}
                      filterValue={this.updateTextFilter}
                      filterStatus={this.updateStatusFilter}
                      filterTypes={this.updateTypesFilter}
                    />
                  </Col>
                  <Col xs={12} md={8}>
                    <div>
                      {
                        this.layersFiltered().map(layer =>
                          <ShowLayer key={layer.id} layer={layer} />
                        )
                      }
                    </div>
                  </Col>
                  <Col xs md={1} />
                </Row>
              </React.Fragment>
              :
              <ServerDown />
          }
        </Container>
        :
        <div>
          <Spinner className="mr-3" animation="border" role="status" />
          <span>Loading...</span>
        </div>
    );
  }
}

export default Home;