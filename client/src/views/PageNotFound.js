import React, { Component } from 'react';
import { Image, Container, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'

class PageNotFound extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: `https://www.pngkit.com/png/full/32-329997_crying-panda-emoji-panda-crying.png`,
            alt: `Page Not Found`,
            secondsLeft: 5,
        };
    }

    componentDidMount() {
        const interval = setInterval(() => {
            if (this.state.secondsLeft <= 0) {
                clearInterval(interval)
                this.props.history.push('/')
            }
            else
                this.setState({ secondsLeft: this.state.secondsLeft - 1 })
        }, 1000)
    }

    render() {
        return (
            <Container className="text-center">
                <Row>
                    <Col xs={12} md={6}>
                        <Image src={this.state.image} alt={this.state.alt} fluid/>
                    </Col>
                    <Col xs={12} md={6} className="align-self-center">
                        <h1>
                            Oh No! We couldn't find the requested page.
                        </h1>
                        <span className="text-muted">
                            You will be redirected to <Link to="/">Home Page</Link> in 
                                <strong> {this.state.secondsLeft}</strong>
                        </span>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default PageNotFound;