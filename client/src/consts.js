export const server = `http://localhost:5000`

const addZeroToDate = (param) => {
    if (param < 10)
      return `0${param}`
    return param
  }

export const getDate = (dateString) => {
    const date = new Date(dateString)
    const day = addZeroToDate(date.getDate())
    const month = addZeroToDate(date.getMonth()+1)
    const year = date.getFullYear()
    const hours = addZeroToDate(date.getHours())
    const minutes = addZeroToDate(date.getMinutes())

    return `${day}/${month}/${year} ${hours}:${minutes}`
}

export const refresh = () => {
  window.location.reload()
}

// uppercase, lowercase, numbers, underscore, hyphen
export const matchSourceID = (val) => {
  return val && /^[a-zA-Z0-9-_]+$/.test(val)
}
// uppercase, lowercase, numbers
export const matchString = (val) => {
  return val && /^[a-zA-Z0-9]+$/.test(val)
}
// numbers
export const matchNumber = (val) => {
  return val && /^[0-9]+$/.test(val)
}

export const matchSentence = (val) => {
  return val && /^[a-zA-Z0-9 ]+$/.test(val)
}

export const getCookie = (name) => {
  let value = "; " + document.cookie;
  let parts = value.split(`; ${name}=`);
  if (parts.length === 2) return parts.pop().split(";").shift();
}

/**
 *     checkLocalStorage = () => {
        const db_types = [`types`, `statuses`, `categories`]

        db_types.forEach(type => {
            if (localStorage.getItem(type) !== null) {
                this.setState({ [type]: localStorage.getItem(type).split(',') })
            }
            else {
                axios.get(`${server}/db_types/${type}`)
                    .then(res => {
                        this.setState({ [type]: res.data })
                        localStorage.setItem(type, res.data)
                    })
            }
        })
    }
 */