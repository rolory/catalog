import React, { Component } from 'react';
import { Row, Col, Image, Table } from 'react-bootstrap'
import { Calendar, Pen, GearWideConnected, Documents, Option, ImageAlt } from 'react-bootstrap-icons'
import { Link } from 'react-router-dom'
import {getDate} from '../consts'

class ShowLayer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Row className="mb-5 border p-5 rounded layer-shadow">
        <Col xs={12} md={3} className="text-left">
          <Link to={`/layer/${this.props.layer.id}`}>
            <Image
              src={this.props.layer.thumbnail}
              alt={this.props.layer.name}
              thumbnail
            />
          </Link>
        </Col>
        <Col xs={12} md={9}>
          <Row>
            <Col xs={12} md={12}>
              <Link to={`/layer/${this.props.layer.id}`}>
                <h1 className="display-sm-3 display-md-3" style={{wordWrap: 'break-word'}}>{this.props.layer.name}</h1>
              </Link>
            </Col>
            <Col xs={12} md={12}>
              <p className="lead">
                {this.props.layer.description}
              </p>
            </Col>
          <Col xs={12} md={12}>
          <Table responsive size="sm">
            <thead>
              <tr>
                <th>
                  <Pen size={20} className="mr-2" />
                  Type
                </th>
                <th>
                  <GearWideConnected size={20} className="mr-2" />
                  Version
                </th>
                <th>
                  <Documents size={20} className="mr-2" />
                  Category
                </th>
                <th>
                  <Option size={20} className="mr-2" />
                  Status
                </th>
                <th>
                  <ImageAlt size={20} className="mr-2" />
                  Reference System
                </th>
                <th>
                  <Calendar size={20} className="mr-2" />
                  Uploaded
                </th>
                <th>
                  <Calendar size={20} className="mr-2" />
                  Modified
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  {this.props.layer.type}
                </td>
                <td>
                  {this.props.layer.version}
                </td>
                <td>
                  {this.props.layer.category}
                </td>
                <td>
                  {this.props.layer.status}
                </td>
                <td>
                  {this.props.layer.geographic_reference_system}
                </td>
                <td>
                  {getDate(this.props.layer.date_range.start)}
                </td>
                <td>
                  {getDate(this.props.layer.last_modified)}
                </td>
              </tr>
            </tbody>
          </Table>
          </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default ShowLayer;