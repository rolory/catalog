import React, { Component } from 'react';
import { Modal } from 'react-bootstrap'
import CreateLayerForm from './CreateLayerForm'

class CenteredModal extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    } 

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"     
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Add a Layer
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <CreateLayerForm onClick={this.props.onHide}/>
                </Modal.Body>
            </Modal>
        );
    }
}

export default CenteredModal;