import React, { Component } from 'react';
import { Card, Form, InputGroup } from 'react-bootstrap'
import { Search } from 'react-bootstrap-icons'
import axios from 'axios'
import { server, getCookie } from '../consts'

class LayersFilter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: null,
            statuses: null,
            types: null
        }
    }

    componentDidMount() {
        ['categories', 'statuses', 'types'].map(db_type => this.checkCookie(db_type))
    }

    checkCookie = (name) => {
        if (document.cookie.indexOf(`${name}=`) > -1) {
            this.setState({ [name]: getCookie(name).split(',') })
        }
        else {
            axios.get(`${server}/db_types/${name}`)
                .then(res => {
                    this.setState({ [name]: res.data })
                    document.cookie = `${name}=${res.data};`
                })
        }
    }

    componentDidLoad = () => {
        return (
            this.state.categories &&
            this.state.statuses &&
            this.state.types
        )
    }

    render() {
        return (
            <div>
                {this.componentDidLoad() ?
                    < Card >
                        <Card.Header>
                            Filters
                        </Card.Header>
                        <Card.Body>
                            <Form.Group>
                                <Form.Label>Search By Name / Description</Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <Search />
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <Form.Control
                                        type="text"
                                        placeholder="Type in..."
                                        onChange={this.props.filterValue}
                                    />
                                </InputGroup>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Categroies</Form.Label>
                                <Form.Control as="select" onChange={this.props.filterCategory}>
                                    <option value="">All</option>
                                    {this.state.categories.map((category, index) =>
                                        <option key={index} value={category}>{category}</option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Statuses</Form.Label>
                                <Form.Control as="select" onChange={this.props.filterStatus}>
                                    <option value="">All</option>
                                    {this.state.statuses.map((status, index) =>
                                        <option key={index} value={status}>{status}</option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Types</Form.Label>
                                <Form.Control as="select" onChange={this.props.filterTypes}>
                                    <option value="">All</option>
                                    {this.state.types.map((type, index) =>
                                        <option key={index} value={type}>{type}</option>
                                    )}
                                </Form.Control>
                            </Form.Group>
                        </Card.Body>
                    </Card>
                    : ``
                }
            </div>
        );
    }
}

export default LayersFilter;