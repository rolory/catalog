import React, { Component } from 'react';
import {ButtonToolbar, Button} from 'react-bootstrap'
import CenteredModal from './Modal'
import {Plus} from 'react-bootstrap-icons'

class CreateLayer extends Component {
    constructor(props) {
        super(props);
        this.state = {modalShow: false};
    }

    setModalShow = (param) => {
        this.setState({modalShow: param})
    }

    render() {
        return (
            <ButtonToolbar>
                <Button size="lg" variant="outline-primary" onClick={() => this.setModalShow(true)}>
                    <Plus size={20} />Add a Layer
                </Button>

                <CenteredModal
                    show={this.state.modalShow}
                    onHide={() => this.setModalShow(false)}
                />
            </ButtonToolbar>
        );
    }
}

export default CreateLayer;