import React, { Component } from 'react';
import {
    Calendar, Pen, FolderFill,
    GearWideConnected, LockFill, Option,
    ImageAlt, Archive, Command, DocumentText
} from 'react-bootstrap-icons'
import { Table, Form } from 'react-bootstrap'
import { getDate, refresh, server, matchNumber, matchString, matchSentence } from '../consts'
import axios from 'axios'
import * as _ from 'lodash'
import SweetAlert from 'react-bootstrap-sweetalert'

class DetailsTable extends Component {
    constructor(props) {
        super(props)
        this.state = {
            layer: {
                ...this.props.layer,
            },
            beforeUpdateLayer: {
                ...this.props.layer
            },
            swal: {
                type: null,
                title: null,
                show: false
            }
        }
    }

    validateUpdate = () => {
    if (!matchString(this.state.layer.name))
        throw new Error("Layer's name must be alphanumerical")

    else if (!matchSentence(this.state.layer.description))
        throw new Error("Layer's description must be alphanumerical")

    else if (!matchNumber(this.state.layer.geographic_reference_system))
        throw new Error("Layer's geographical reference system must be an integer")

    else if (!matchString(this.state.layer.source))
        throw new Error("Layer's source must be alphanumerical")

    else if (!matchNumber(this.state.layer.version))
        throw new Error("Layer's version must be an integer")

    else
        return true
    }

    setSwal = (type, title, show) => {
        this.setState({
            swal: { type, title, show }
        })
    }

    updateLayer = () => {
        try {
            this.validateUpdate()
            
            // Check if the layer has really changed
            // This prevents sending PATCH requests for a not-edited layer
            if(!_.isEqual(this.state.beforeUpdateLayer, this.state.layer)) {
                axios.patch(`${server}/layer/${this.state.layer.id}`, this.state.layer)
                    .then(() => {
                        this.setSwal('success', 
                        <span>Layer 
                            <span className={`text-success font-italic`}> {this.state.layer.name} </span> 
                        updated</span>, true)
                        setTimeout(() => refresh(), 500)
                    })
                    .catch(() =>
                        this.setSwal('danger', `We couldn't update layer ${this.state.layer.name}`, true)
                    )
            }
            return true
        } catch (e) {
            this.setSwal('danger', e.message , true)
            return false
        }
    }

    handleChange = (event, key, ...nestedProperties) => {
        const value = event.target.value
        let objectClone = _.clone(this.state.layer[key])

        // Handle when changing a nested object value

        if (nestedProperties.length) {
            nestedProperties.forEach(p => {
                _.set(objectClone, p, value)
            })
            this.setState({layer: {...this.state.layer,[key]: objectClone}})
        } else {
            this.setState({layer: {...this.state.layer,[key]: value}})
        }
    }

    hideAlert = () => {
        this.setState({
            swal: {type: null, title: null, show: false}
        })
    }

    render() {
        return (
            <React.Fragment>
                <Table className="layers-table" striped bordered hover>
                    <tbody>
                        <tr>
                            <th>
                                <LockFill size={20} className="mr-2" />
                                ID
                        </th>
                            <td>
                                <Form.Control
                                    readOnly plaintext
                                    value={this.state.layer.source_id} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <DocumentText size={20} className="mr-2" />
                                Name
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'name')}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.name} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <FolderFill size={20} className="mr-2" />
                                Description
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'description')}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.description} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <ImageAlt size={20} className="mr-2" />
                                Reference System
                        </th>
                            <td>
                                <Form.Control
                                    onChange={(event) => this.handleChange(event, 'geographic_reference_system')}
                                    type="number"
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.geographic_reference_system} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Calendar size={20} className="mr-2" />
                                Uploaded
                        </th>
                            <td>
                                <Form.Control
                                    readOnly plaintext
                                    defaultValue={getDate(this.props.layer.date_range.start)} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Calendar size={20} className="mr-2" />
                                Modified
                        </th>
                            <td>
                                <Form.Control
                                    readOnly plaintext
                                    defaultValue={getDate(this.props.layer.last_modified)} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Pen size={20} className="mr-2" />
                                Type
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'type')}
                                    as={this.props.editMode ? "select" : "input"}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.type}>
                                    {this.props.editMode ?
                                        this.props.types.map((status, index) =>
                                            <option key={index}>{status}</option>)
                                        : null}
                                </Form.Control>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Archive size={20} className="mr-2" />
                                Source
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'source')}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.source} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Command size={20} className="mr-2" />
                                Connection Protocol
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'connection_metadata', 'protocol')}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.connection_metadata.protocol} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <GearWideConnected size={20} className="mr-2" />
                                Version
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'version')} type="number"
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.version} />
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <Option size={20} className="mr-2" />
                                Status
                        </th>
                            <td>
                                <Form.Control onChange={(event) => this.handleChange(event, 'status')}
                                    as={this.props.editMode ? "select" : "input"}
                                    readOnly={!this.props.editMode} plaintext={!this.props.editMode}
                                    value={this.state.layer.status}>
                                    {this.props.editMode ?
                                        this.props.statuses.map((status, index) =>
                                            <option key={index}>{status}</option>)
                                        : null}
                                </Form.Control>
                            </td>
                        </tr>
                    </tbody>
                </Table>
                {
                    this.state.swal.show ?
                        <SweetAlert
                            type={this.state.swal.type}
                            title={this.state.swal.title}
                            onConfirm={this.hideAlert}
                        />
                        : null
                }
            </React.Fragment>
        );
    }
}

export default DetailsTable;