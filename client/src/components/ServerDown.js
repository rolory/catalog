import React, { Component } from 'react';
import { Alert, Row, Col, Image } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { refresh } from '../consts'
import axios from 'axios'

class ServerDown extends Component {
    constructor(props) {
        super(props);
        this.state = { loaded: false, seconds: 1 };
    }

    componentDidMount() {
        this.checkServerStatus()
    }

    componentDidMount = () => {
        const interval = setInterval(async () => {
            try {
                await axios.get(`http://localhost:5000/`)
            } catch (e) {
                if (e.response) {
                    this.setState({ loaded: true })
                    clearInterval(interval)
                }
            }
        }, this.state.seconds * 1000)
    }

    refreshPage = () => {
        setTimeout(() => {
            window.location.reload()
        }, 700)
    }

    render() {
        return (
            <React.Fragment>
                <Row xs={12} md={12} className="text-center">
                    <Col xs={12} md={3} />
                    <Col xs={12} md={6}>
                        <Row xs={12} md={12}>
                            <Col xs={12} md={12}>
                                <Alert variant="dark">
                                    <Alert.Heading>
                                        <strong>
                                            Could not retrieve data from the server
                            </strong>
                                    </Alert.Heading>
                                    <p>
                                        Don't panic, we usually fix this within a short amount of time.
                                    </p>
                                    <hr />
                                    <p>
                                        Meanwhile, you can enjoy this panda GIF below, or just
                                        <Link to="/" onClick={() => refresh()}> reload the page</Link> to check
                                                        for updates.
                                    </p>
                                    <p>
                                        <Image fluid alt="Panda" src="https://media0.giphy.com/media/EPcvhM28ER9XW/giphy.gif" />
                                    </p>
                                    <p>

                                        Server status:
                                        <strong>
                                            {
                                                this.state.loaded ?
                                                    <React.Fragment>
                                                        <span className="text-success"> Up!</span> <br />
                                                        Reloading... {this.refreshPage()}
                                                    </React.Fragment>
                                                    :
                                                    <span className="text-danger"> Down</span>
                                            }
                                        </strong>
                                    </p>
                                </Alert>
                            </Col>
                            <Col xs={12} md={12}>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                <Col xs={12} md={3} />
            </React.Fragment>
        );
    }
}

export default ServerDown