import React, { Component } from 'react';
import { Form, Col, Spinner, Button } from 'react-bootstrap'
import axios from 'axios'
import { server, matchSourceID, matchString, matchNumber, matchSentence } from '../consts'
import * as _ from 'lodash'
import SweetAlert from 'react-bootstrap-sweetalert'
import { withRouter } from 'react-router'

class CreateLayerForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            types: null,
            statuses: null,
            categories: null,
            layer: {
                thumbnail: null,
                source_id: '',
                name: null,
                description: null,
                source: null,
                geographic_extent: null,
                geographic_reference_system: null,
                type: null,
                category: null,
                status: null,
                connection_metadata: {
                    protocol: null,
                    capabilitiesUrl: null,
                },
                version: null,
                last_modified: null,
                date_range: {
                    start: null,
                    end: null,
                }
            },
            swal: {
                type: null,
                title: null,
                show: false
            },
            layerDB_ID: null
        };
    }


    componentDidMount() {
        axios.get(`${server}/db_types/types`)
            .then(res => this.setState({ types: res.data }))

        axios.get(`${server}/db_types/statuses`)
            .then(res => this.setState({ statuses: res.data }))

        axios.get(`${server}/db_types/categories`)
            .then(res => this.setState({ categories: res.data }))
    }

    componentDidLoad = () => {
        return (
            this.state.types &&
            this.state.statuses &&
            this.state.categories
        )
    }

    imageToURL = (file) => new Promise((resolve, reject) => {
        const reader = new FileReader()
        const image = new Image()
        reader.readAsDataURL(file)
        reader.onload = () => {
            image.onload = () => resolve(reader.result)
            image.onerror = error => reject(error)
            image.src = reader.result
        }
        reader.onerror = error => reject(error)
    })

    fileToText = (file) => new Promise((resolve, reject) => {
        const reader = new FileReader()
        reader.readAsText(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });

    validateForm = () => {
        if (!matchSourceID(this.state.layer.source_id))
            throw new Error("Layer's sourceID must contain letters/numbers/underscore/hyphen values")

        else if (!matchString(this.state.layer.name))
            throw new Error("Layer's name must be alphanumerical")

        else if (!matchSentence(this.state.layer.description))
            throw new Error("Layer's description must be alphanumerical")

        else if (!matchNumber(this.state.layer.geographic_reference_system))
            throw new Error("Layer's geographical reference system must be an integer")

        else if (!this.state.types.includes(this.state.layer.type))
            throw new Error(`Please fill layer's type`)

        else if (!this.state.categories.includes(this.state.layer.category))
            throw new Error(`Please fill layer's category`)

        else if (!this.state.statuses.includes(this.state.layer.status))
            throw new Error(`Please fill layer's status`)

        else if (!matchString(this.state.layer.source))
            throw new Error("Layer's source must be alphanumerical")

        else if (!matchNumber(this.state.layer.version))
            throw new Error("Layer's version must be an integer")

        else
            return true
    }

    handleChange = async (event, key) => {
        let layer = _.clone(this.state.layer)
        _.set(layer, key, event.target.value)
        await this.setState({ layer })
    }

    handleThumbnailChange = async (event) => {
        try { 
            let layer = _.clone(this.state.layer)
            const url = await this.imageToURL(event.target.files[0])
            _.set(layer, 'thumbnail', url)
            await this.setState({ layer })
        } catch (e) {
            this.setSwal('danger', `You must upload a valid image file.`, true)
        }
    }

    handleJSONChange = async (event) => {
        try {
            let layer = _.clone(this.state.layer)
            const url = JSON.parse(await this.fileToText(event.target.files[0]))
            _.set(layer, 'geographic_extent', url)
            await this.setState({ layer })
        } catch (e) {
            this.setSwal('danger', `You must upload a valid JSON file.`, true)
        }
    }

    handleSubmit = async (event) => {
        // Assign current date 
        const layer = _.clone(this.state.layer)
        layer.last_modified = 
        layer.date_range.start = layer.date_range.end = new Date()


        await this.setState({ layer })

        try {
            this.validateForm()

            axios.post(`${server}/layer`, this.state.layer)
                .then((res) => {
                    this.setSwal('success', `Layer ${this.state.layer.name} succesfully created!`, true)
                    this.setState({ layerDB_ID: res.data.id })
                })
                .catch(() => this.setSwal('danger', `One or more fields is not valid.`, true))

        } catch (e) {
            this.setSwal('danger', e.message, true)
        }
    }

    hideAlert = () => {
        const type = this.state.swal.type

        this.setState({
            swal: {
                type: null,
                title: null,
                show: false
            }
        })

        if (type === 'success')
            this.props.history.push(`/layer/${this.state.layerDB_ID}`)
    }

    setSwal = (type, title, show) => {
        this.setState({
            swal: { type, title, show }
        })
    }

    render() {
        return (
            this.componentDidLoad() ?
                <Form>
                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Layer Thumbnail</Form.Label>
                            <Form.Control
                            onChange={(event) => this.handleThumbnailChange(event)}
                                type="file" accept="image/*" />
                        </Form.Group>

                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Source ID</Form.Label>
                            <Form.Control 
                            onChange={(event) => this.handleChange(event, 'source_id')}
                                type="text" placeholder="Enter source id" />
                        </Form.Group>

                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Layer Name</Form.Label>
                            <Form.Control
                            onChange={(event) => this.handleChange(event, 'name')}
                                type="text" placeholder="Enter layer name" />
                        </Form.Group>
                    </Form.Row>


                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={12}>
                            <hr />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={12}>
                            <Form.Label>Description</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'description')}
                                type="text" placeholder="Enter layer description" />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={6}>
                            <Form.Label>Geogrpahic Extent (GeoJSON)</Form.Label>
                            <Form.Control onChange={(event) => this.handleJSONChange(event)} type="file" accept=".json" />
                            <Form.Text className="text-muted">
                                Upload your <strong>GeoJSON</strong> geographic extent file
                        </Form.Text>
                        </Form.Group>

                        <Form.Group as={Col} xs={12} md={6}>
                            <Form.Label>Geogrpahic Reference System</Form.Label>
                            <Form.Control
                                onChange={(event) => this.handleChange(event, 'geographic_reference_system')}
                                type="number" placeholder="Enter geo reference system" />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={12}>
                            <hr />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Layer Type</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'type')}
                                as="select">
                                <option value="">Choose...</option>
                                {
                                    this.state.types.map((type, index) =>
                                        <option key={index}>{type}</option>
                                    )}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Layer Category</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'category')}
                                as="select">
                                <option value="">Choose...</option>
                                {
                                    this.state.categories.map((category, index) =>
                                        <option key={index}>{category}</option>
                                    )}
                            </Form.Control>
                        </Form.Group>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Layer Status</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'status')}
                                as="select">
                                <option value="">Choose...</option>
                                {
                                    this.state.statuses.map((status, index) =>
                                        <option key={index}>{status}</option>
                                    )}
                            </Form.Control>
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Source</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'source')}
                                type="text" placeholder="Enter source" />
                        </Form.Group>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Connection Metadata - Protocol</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'connection_metadata.protocol')}
                                type="text" placeholder="Enter protocol" />
                        </Form.Group>
                        <Form.Group as={Col} xs={12} md={4}>
                            <Form.Label>Connection Metadata - URL</Form.Label>
                            <Form.Control
                                onChange={(event) => this.handleChange(event, 'connection_metadata.capabilitiesUrl')}
                                type="text" placeholder="Enter URL" />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={12}>
                            <Form.Label>Version</Form.Label>
                            <Form.Control onChange={(event) => this.handleChange(event, 'version')}
                                type="number" placeholder="Enter version" />
                        </Form.Group>
                    </Form.Row>

                    <Form.Row>
                        <Form.Group as={Col} xs={12} md={6} className="text-right">
                            <Button variant="outline-primary" onClick={this.props.onClick}>Close</Button>
                        </Form.Group>
                        <Form.Group as={Col} xs={12} md={6} className="text-left">
                            <Button onClick={this.handleSubmit}>Create Layer</Button>
                        </Form.Group>
                    </Form.Row>
                    {
                        this.state.swal.show ?
                            <SweetAlert
                                type={this.state.swal.type}
                                title={this.state.swal.title}
                                onConfirm={this.hideAlert}
                                closeOnClickOutside
                            />
                            : null
                    }
                </Form>
                :
                <React.Fragment>
                    <Spinner className="mr-3" animation="border" role="status" />
                    <span>Loading...</span>
                </React.Fragment>
        );
    }
}

export default withRouter(CreateLayerForm);