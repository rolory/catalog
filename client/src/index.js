import React from 'react';
import ReactDOM from 'react-dom';
import Home from './views/Home.js';
import Layer from './views/Layer.js';
import * as serviceWorker from './serviceWorker';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Switch, Route, BrowserRouter as Router, Redirect} from 'react-router-dom'
import './styles/generalStyles.css'
import PageNotFound from './views/PageNotFound.js';

const routing = (
    <Router>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/layer/:id" component={Layer}/>
            <Route exact path="/404" component={PageNotFound}/>
            <Route component={PageNotFound} />
            <Redirect to="/404" />
        </Switch>
    </Router>
)

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
